const data = require("./data.js");

// Import function error handler
const errorHandler = require("./errorHandler.js");

function filterData2 (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if ((data.gender === "female" || data.company === "FSW4") && data.age > 30){
        result.push(data);
    }
    
})

return (result.length < 1) ? errorHandler() : result;
}

module.exports = filterData2(data)
