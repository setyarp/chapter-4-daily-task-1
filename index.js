// import atau panggil dotenv module (third party module)
require('dotenv').config()

// console.log(dataPerson)
/**
 * Impor HTTP Standar Library dari Node.js
 * Hal inilah yang nantinya akan kita gunakan untuk membuat
 * HTTP Server
 * */
 const http = require('http');

const fs = require('fs');
const path = require('path');

// Import
const filterOne = require("./filterOne.js");
const filterTwo = require('./filterTwo.js');
const filterThree = require('./filterThree.js');
const filterFour = require('./filterFour.js');
const filterFive = require('./filterFive.js');


// assign directory path dan deklarasi function untuk panggil html
const PUBLIC_HTML = path.join(__dirname, 'public');
function getHTML(htmlFileName) {
    const htmlFilePath = path.join(PUBLIC_HTML, htmlFileName);
    return fs.readFileSync(htmlFilePath, 'utf-8')
}

// assign directory path dan deklarasi function untuk panggil css
const PUBLIC_CSS = path.join(__dirname, "public");
function getCSS(cssFileName) {
  const cssFilePath = path.join(PUBLIC_CSS, cssFileName);
  return fs.readFileSync(cssFilePath, { encoding: "utf8" });
}

// assign directory path dan deklarasi function untuk panggil image
function getIMG(imgFileName) {
  const PUBLIC_IMG = path.join(__dirname, "public");
  const imgFilePath = path.join(PUBLIC_IMG, imgFileName);
  return imgFilePath
}

function toJSON(value) {
  return JSON.stringify(value);
}
 // Request handler
 // Fungsi yang berjalan ketika ada request yang masuk.
 function onRequest(req, res) {
  switch(req.url) {
    case "/":
      res.writeHead(200)
      res.end(getHTML("index.html"))
      return;
    case "/about":
      res.writeHead(200)
      res.end(getHTML("about.html"))
      return;
    case "/data1":
      const filter1 = toJSON(filterOne);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter1);
      return;
    case "/data2":
      const filter2 = toJSON(filterTwo);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter2);
      return;
    case "/data3":
      const filter3 = toJSON(filterThree);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter3);
      return;
    case "/data4":
      const filter4 = toJSON(filterFour);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter4);
      return;
    case "/data5":
      const filter5 = toJSON(filterFive);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter5);
      return;
    case "/style.css":
      res.writeHead(200, { "Content-type": "text/css" });
      res.write(getCSS("/style.css"));
      res.end();
      return;
    case "/not_found.png":
      fs.readFile(getIMG("/not_found.png"), function(err, data) {
      if (err) throw err // Fail if the file can't be read.
      res.writeHead(200, {'Content-Type': 'image/jpeg'})
      res.end(data) 
  })
      return;

    default:
      res.writeHead(404)
      res.end(getHTML("404.html"))
      return;
  }
 }
 
 const server = http.createServer(onRequest);
 
 // Jalankan server dengan port 3000 dari .env file
 server.listen(process.env.PORT, () => {
   console.log("Server sudah berjalan, silahkan buka http://localhost:3000");
 })